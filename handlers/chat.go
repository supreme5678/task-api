package handlers

// var pusherClient = pusher.Client{
// 	AppID:   "1016166",
// 	Key:     "e7da5c3c897963bc3aff",
// 	Secret:  "0bbdcd77e2fdf3e702a5",
// 	Cluster: "ap1",
// 	Secure:  true,
// }

// type user struct {
// 	Name  string `json:"name" xml:"name" form:"name" query:"name"`
// 	Email string `json:"email" xml:"email" form:"email" query:"email"`
// }

// func RegisterNewUser(rw http.ResponseWriter, req *http.Request) {
// 	body, err := ioutil.ReadAll(req.Body)
// 	if err != nil {
// 		panic(err)
// 	}

// 	var newUser user

// 	err = json.Unmarshal(body, &newUser)
// 	if err != nil {
// 		panic(err)
// 	}

// 	pusherClient.Trigger("update", "new-user", newUser)

// 	json.NewEncoder(rw).Encode(newUser)
// }

// func PusherAuth(res http.ResponseWriter, req *http.Request) {
// 	params, _ := ioutil.ReadAll(req.Body)
// 	response, err := pusherClient.AuthenticatePrivateChannel(params)
// 	if err != nil {
// 		panic(err)
// 	}

// 	fmt.Fprintf(res, string(response))
// }

// func RegisterNewUser() gin.HandlerFunc {
// 	return func(c *gin.Context) {
// 		body, err := ioutil.ReadAll(c.Request.Body)
// 		if err != nil {
// 			panic(err)
// 		}

// 		var newUser user

// 		err = json.Unmarshal(body, &newUser)
// 		if err != nil {
// 			panic(err)
// 		}

// 		pusherClient.Trigger("update", "new-user", newUser)

// 		json.NewEncoder(c.Writer).Encode(newUser)
// 	}

// }

// func PusherAuth() gin.HandlerFunc {
// 	return func(c *gin.Context) {
// 		params, _ := ioutil.ReadAll(c.Request.Body)
// 		response, err := pusherClient.AuthenticatePrivateChannel(params)
// 		if err != nil {
// 			panic(err)
// 		}

// 		fmt.Fprintf(c.Writer, string(response))
// 	}

// }
