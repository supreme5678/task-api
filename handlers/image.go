package handlers

import (
	"net/http"
	"os"
	"strconv"
	"task-api/models"

	"github.com/gin-gonic/gin"
)

type ImageHandler struct {
	ig *models.ImageGorm
}

func CreateImageHandler(ig *models.ImageGorm) *ImageHandler {
	return &ImageHandler{ig}
}

func (ih *ImageHandler) AddImage(c *gin.Context) {
	newImg := models.NewImage{}
	strID := c.Param("id")

	form, err := c.MultipartForm()
	if err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}

	file := form.File["photo"][0]
	os.MkdirAll("./upload/"+strID, os.ModePerm)
	err = c.SaveUploadedFile(file, "./upload/"+strID+"/"+file.Filename)
	if err != nil {
		c.JSON(501, gin.H{
			"message": err.Error(),
		})
	}

	newImg.FileName = file.Filename

	img, err := ih.ig.AddImage(newImg, strID)
	if err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(http.StatusOK, gin.H{
		"image": img,
		// "path":  path.Join("localhost:8080", "images", strID, file.Filename),
	})
}

func (ih *ImageHandler) DeleteImage(c *gin.Context) {
	strID := c.Param("id")
	id, err := strconv.Atoi(strID)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
	}
	if err := ih.ig.Delete(uint(id)); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
	}
	c.Status(http.StatusOK)
}
