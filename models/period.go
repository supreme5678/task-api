package models

type Period struct {
	DateID   uint `json:"date_id`
	StartDay bool `json:"startDay"`
	EndDay   bool `json:"endDay"`
}
