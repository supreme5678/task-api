package models

import (
	"github.com/jinzhu/gorm"
)

type Chat struct {
	gorm.Model
	UserID   uint   `json:"user_id"`
	FriendID uint   `json:"friend_id"`
	Channel  string `json:"channel"`
}

type CreateChat struct {
	UserID   uint   `json:"user_id"`
	FriendID uint   `json:"friend_id"`
	Channel  string `json:"channel"`
}
