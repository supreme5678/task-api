package models

import (
	"log"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strconv"

	"github.com/jinzhu/gorm"
)

type ImageService interface {
	AddImage(img *Image) error
}
type Image struct {
	gorm.Model
	FileName string `json:"image"`
	Src      string `json:"src"`
	UserID   uint   `json:"user_id"`
}

type NewImage struct {
	FileName string `json:"image"`
	Src      string `json:"src"`
	UserID   uint   `json:"user_id"`
}

type ImageGorm struct {
	db *gorm.DB
}

func CreateImageService(db *gorm.DB) *ImageGorm {
	return &ImageGorm{db}
}

func (ig *ImageGorm) AddImage(image NewImage, strID string) (*Image, error) {
	id, err := strconv.Atoi(strID)
	if err != nil {
		log.Fatal(err)
	}
	newImg := new(Image)
	newImg.FileName = image.FileName
	newImg.UserID = uint(id)

	// u := "http://localhost:8080"

	u, err := url.Parse("http://192.168.1.6:8080")
	if err != nil {
		log.Fatal(err)
	}
	u.Path = path.Join(u.Path, "images", strID, image.FileName)
	s := u.String()
	newImg.Src = s

	if err := ig.db.Create(newImg).Error; err != nil {
		return newImg, err
	}
	return newImg, nil
}

func (ig *ImageGorm) GetByID(id uint) (*Image, error) {
	img := new(Image)
	err := ig.db.First(img, "id = ?", id).Error
	if err != nil {
		return nil, err
	}
	return img, nil
}

func (ig *ImageGorm) Delete(id uint) error {
	img, err := ig.GetByID(id)
	if err != nil {
		return err
	}
	err = os.Remove(filepath.Join("upload", strconv.FormatUint(uint64(id), 10), img.FileName))
	if err != nil {
		log.Printf("Fail deleting image: %v\n", err)
	}
	return ig.db.Where("id = ?", id).Delete(&Image{}).Error

}
