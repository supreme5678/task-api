package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

type Task struct {
	gorm.Model
	TaskName string    `json:"taskName"`
	Tag      string    `json:"tag"`
	UserID   uint      `json:"user_id"`
	Location Location  `json:"location"`
	IsDone   bool      `json:"isDone"`
	Date     time.Time `json:"date" sql:"datetime(6)"`
	Note     string    `json:"note"`
}

type NewTask struct {
	UserID   uint      `json:"user_id"`
	TaskName string    `json:"taskName"`
	Tag      string    `json:"tag"`
	IsDone   bool      `json:"isDone"`
	Date     time.Time `json:"date" sql:"datetime(6)"`
	Location Location  `json:"location"`
	Note     string    `json:"note"`
}
