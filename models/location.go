package models

type Location struct {
	LocationName string  `json:"locationName"`
	TaskID       uint    `json:"taskId'`
	Lat          float32 `json:"lat"`
	Long         float32 `json:"long"`
}
