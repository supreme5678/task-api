package main

import (
	"log"
	"net/http"
	"strconv"
	"task-api/config"
	"task-api/handlers"
	"task-api/models"
	"task-api/mw"

	"time"

	"github.com/gin-contrib/cors"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/pusher/pusher-http-go"

	"github.com/gin-gonic/gin"
)

var pusherClient = pusher.Client{
	AppID:   "1016166",
	Key:     "e7da5c3c897963bc3aff",
	Secret:  "0bbdcd77e2fdf3e702a5",
	Cluster: "ap1",
	Secure:  true,
}

type user struct {
	Name  string `json:"name" xml:"name" form:"name" query:"name"`
	Email string `json:"email" xml:"email" form:"email" query:"email"`
}

func main() {
	conf := config.Load()

	db, err := gorm.Open("mysql", conf.Connection)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if conf.Mode == "dev" {
		db.LogMode(true)
	}

	if err = db.AutoMigrate(&models.Task{}, &models.Period{}, &models.Location{}, &models.User{}, &models.Image{}).Error; err != nil {
		log.Fatal(err)
	}

	//GIN
	r := gin.Default()

	config := cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"*"},
		AllowHeaders:     []string{"*"},
		ExposeHeaders:    []string{"*"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}
	r.Use(cors.New(config))

	r.Static("/images", "./upload")

	ug := models.CreateUserService(db, conf.HMACKey)
	uh := handlers.CreateUserHandler(ug)

	ig := models.CreateImageService(db)
	ih := handlers.CreateImageHandler(ig)

	if conf.Mode != "dev" {
		gin.SetMode(gin.ReleaseMode)
	}

	// ----------------------------CHAT----------------------------

	// data := map[string]string{"message": "hello world"}
	// PusherClient.Trigger("my-channel", "my-event", data)

	// r.Static("/chat", "./public")
	// r.POST("/chat/new/user", func(c *gin.Context) {
	// 	body, err := ioutil.ReadAll(c.Request.Body)
	// 	if err != nil {
	// 		panic(err)
	// 	}

	// 	var newUser models.User

	// 	err = json.Unmarshal(body, &newUser)
	// 	if err != nil {
	// 		panic(err)
	// 	}

	// 	pusherClient.Trigger("my-chanel", "new-user", newUser)

	// 	json.NewEncoder(c.Writer).Encode(newUser)
	// })
	// r.POST("/chat/pusher/auth", func(c *gin.Context) {
	// 	params, _ := ioutil.ReadAll(c.Request.Body)
	// 	response, err := pusherClient.AuthenticatePrivateChannel(params)
	// 	if err != nil {
	// 		panic(err)
	// 	}

	// 	fmt.Fprintf(c.Writer, string(response))
	// })

	// ----------------------------USER----------------------------
	r.POST("/signup", uh.SignUp)
	r.POST("/signin", uh.SignIn)
	// r.GET("/getuserby/:id", uh.GetUsernameByID)

	// ----------------------------TASK----------------------------

	protec := r.Group("/")
	protec.Use(cors.New(config), mw.AuthenticationRequired(ug))
	{
		protec.GET("/sessions", func(c *gin.Context) {
			user, ok := c.Value("user").(*models.User)
			if !ok {
				c.JSON(401, gin.H{
					"message": "invalid token",
				})
			}
			c.JSON(http.StatusOK, user)
		})

		protec.POST("/images/:id/image", ih.AddImage)
		protec.DELETE("/images/:id/image", ih.DeleteImage)

		protec.POST("/user/:id/task", func(c *gin.Context) {
			//Query id
			userID := c.Param("id")
			id, err := strconv.Atoi(userID)
			if err != nil {
				c.JSON(404, gin.H{
					"message": err.Error(),
				})
			}
			//Bind JSON
			newTask := models.NewTask{}
			if err := c.BindJSON(&newTask); err != nil {
				c.JSON(404, gin.H{
					"message": err.Error(),
				})
			}

			task := models.Task{}
			task.IsDone = false
			task.UserID = uint(id)
			task.Tag = newTask.Tag
			task.TaskName = newTask.TaskName
			task.Date = newTask.Date
			task.Note = newTask.Note
			if err := db.Create(&task).Error; err != nil {
				c.JSON(404, gin.H{
					"message": err,
				})
			}

			lo := models.Location{}
			lo.LocationName = newTask.Location.LocationName
			lo.Lat = newTask.Location.Lat
			lo.Long = newTask.Location.Long
			lo.TaskID = task.ID

			if err := db.Create(&lo).Error; err != nil {
				c.JSON(404, gin.H{
					"message": err,
				})
			}

			c.JSON(http.StatusOK, newTask)
		})

		// Get one task
		protec.GET("/user/:id/task", func(c *gin.Context) {
			userID := c.Param("id")
			id, err := strconv.Atoi(userID)
			if err != nil {
				c.JSON(404, gin.H{
					"message": err.Error(),
				})
			}
			task := models.Task{}
			if err := db.Where("user_id = ? AND date > NOW() AND date < ADDDATE(NOW(),INTERVAL 1 DAY) ", uint(id)).Order("date asc").First(&task).Error; err != nil {
				c.JSON(404, gin.H{
					"message": err.Error(),
				})
			}

			lo := models.Location{}
			err = db.Where("task_id = ?", task.ID).Find(&lo).Error
			if err != nil {
				c.JSON(404, gin.H{
					"message": err.Error(),
				})
			}
			task.Location = lo

			c.JSON(http.StatusOK, task)

		})

		// Get today tasks
		protec.GET("/user/:id/today", func(c *gin.Context) {
			userID := c.Param("id")
			id, err := strconv.Atoi(userID)
			if err != nil {
				c.JSON(404, gin.H{
					"message": err.Error(),
				})
			}
			task := []models.Task{}
			if err := db.Where("user_id = ? AND date >= NOW() AND date < DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 1 DAY)", uint(id)).Order("date asc").Limit(10).Find(&task).Error; err != nil {
				c.JSON(404, gin.H{
					"message": err.Error(),
				})
			}

			for i := 0; i < len(task); i++ {
				lo := models.Location{}
				err = db.Where("task_id = ?", task[i].ID).Find(&lo).Error
				if err != nil {
					c.JSON(404, gin.H{
						"message": err.Error(),
					})
				}
				task[i].Location = lo
			}
			c.JSON(http.StatusOK, task)

		})
		//Get all task
		protec.GET("/user/:id/tasks", func(c *gin.Context) {
			userID := c.Param("id")
			id, err := strconv.Atoi(userID)
			if err != nil {
				c.JSON(404, gin.H{
					"message": err.Error(),
				})
			}
			task := []models.Task{}
			if err := db.Where("user_id = ?", uint(id)).Order("date asc").Find(&task).Error; err != nil {
				c.JSON(404, gin.H{
					"message": err.Error(),
				})
			}
			for i := 0; i < len(task); i++ {
				lo := models.Location{}
				err := db.Where("task_id = ?", task[i].ID).Find(&lo).Error
				if err != nil {
					c.JSON(404, gin.H{
						"message": err.Error(),
					})
				}
				task[i].Location = lo
			}
			c.JSON(http.StatusOK, task)
		})
	}

	protec.DELETE("/task/:task_id", func(c *gin.Context) {
		taskID := c.Param("task_id")
		id, err := strconv.Atoi(taskID)
		if err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
		}
		if err := db.Where("id = ?", uint(id)).Delete(&models.Task{}).Error; err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
		}
		c.Status(http.StatusOK)
	})

	protec.PATCH("/user/:id", uh.UpdateImagePath)
	protec.PATCH("/user/:id/profile", uh.UpdateInfoPath)
	//Create task

	// ----------------------------CHAT----------------------------

	protec.POST("/user/chat/:id", func(c *gin.Context) {
		strID := c.Param("task_id")
		id, err := strconv.Atoi(strID)
		if err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
		}

		newChat := models.CreateChat{}
		if err := c.BindJSON(&newChat); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": "form failed",
			})
			return
		}

		chat := models.Chat{}
		chat.UserID = uint(id)
		chat.FriendID = newChat.FriendID
		chat.Channel = newChat.Channel

		if err := db.Create(&chat).Error; err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
		}
		c.Status(http.StatusOK)
	})

	r.Run()
}

func inTimeSpan(start, end, check time.Time) bool {
	return check.After(start) && check.Before(end)
}
